package com.devcamp.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.models.Customer;
import com.devcamp.customerinvoiceapi.services.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerController {
    @Autowired
    CustomerService customerService;
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomersApi(){
        return customerService.getAllCustomers();
    }
}
